myApp.factory("selectFactory",function() {
  hombre="hombre";
  mujer="mujer";
  casado="casado";
  casada="casada";
  soltero="soltero";
  soltera="soltera";
  return{
    datos:[
      {
        id:1,
        sexo:hombre,
        estado:casado
      },
      {
        id:2,
        sexo:mujer,
        estado:casada
      },
      {
        id:3,
        sexo:hombre,
        estado:soltero
      },
      {
        id:4,
        sexo:mujer,
        estado:soltera
      }
    ]
  };
});
myApp.controller("selectController",["$scope","selectFactory",function ($scope,selectFactory) {
  $scope.collection=selectFactory.datos;
}]);

myApp.directive("mySelect",function($compile) {
  return{
    restrict:'E',
    scope:{
      model:"="
    },
    template:function (element,attrs) {
      // attrs.model=attrs.collection[attrs.default];
      return "<select ng-model="+attrs.model+"><option ng-selected='"+attrs.default+"===($index+1)' ng-repeat='item in "+attrs.collection+"' value='item.id'>{{item.sexo}} - {{item.estado}}</option></select>";
      // return $compile();
    },
    replace:true
  };
});
