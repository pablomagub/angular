myApp.controller("saludos",['$scope', function($scope){
  function saludar() {
    console.log("hola mundo");
  }
  saludar();

  // var foo = function(arg1,arg2) {};
  //
  // var injector = angular.element($0).injector();
  //
  // injector.annotate(foo);
  //
  // foo.$inject;
}]);

myApp.controller("controller1",["$scope","$rootScope",function($scope,$rootScope){
  $scope.f1= function() {
    console.log("llamando");
    $rootScope.$broadcast("f2");
  }
}]);

myApp.controller("controller2",["$scope",function($scope) {
  $scope.$on("f2",function (){
      $scope.text="he sido invocado desde mi hermano";
      console.log("llamado desde el primer controlador");
    }
  );
}]);

myApp.controller("binding",["$scope",function($scope){
  $scope.nombreCompleto= function(){
    return (($scope.campo1 || '') + ' ' + ($scope.campo2 || ''));
  }
}]);

myApp.controller("servicios",["$scope","myFactory","myService","myProvider",function($scope,myFactory,myService,myProvider){
  myFactory.write();
  myService.write();
  myProvider.write();
}]);


myApp.controller("jsoncontroller",["$scope","userProvider","$log",function($scope,userProvider,$log) {
  userProvider.usuarios.success(function(response) {
    //$log.info(response.records);
    $scope.usuarios = response.records;
  })
  .error(function() {
    console.log("error");
  });
}]);

myApp.controller("isolatedEvalController",["$scope",function($scope) {
  $scope.name="Hello world from isoeval controller";
  $scope.click=function() {
    console.log("function isolatedEvalController");
  }
}]);
