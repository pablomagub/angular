myApp.directive('myDirective',['$http',function ($http) {
  console.log('funcion inyectable descde directiva');
  return {
    restric: 'E',
  };
}]);

myApp.directive('myDirectivedos',['$http',function ($http) {
  console.log('funcion inyectable descde directiva 2');
  return {
    restric: 'E',
    link:function () { //No inyectable (scope, element ,attributes)
      console.log("funcion link directiva 2");
      console.log(arguments);
    }
  };
}]);

myApp.directive('myDirectivetres',['$http',function ($http) {
  console.log('funcion inyectable descde directiva 3');
  return {
    restric: 'E',
    link:function () {//No inyectable (scope , element ,attributes)
      console.log("funcion link directiva 3");
    },
    compile:function () { //No inyectable (element ,attributes)
      console.log("funcion compile directiva 3");
      console.log(arguments);
      return function () { //NO inyectable (scope , element , attributes)
        console.log("función link desde compile directiva 3");
        console.log(arguments);
      };
    }
  };
}]);

myApp.directive('myDirectivecuatro',['$http',function ($http) {
  console.log('funcion inyectable descde directiva 4');
  return {
    restric: 'E',
    link:function () {//No inyectable (scope , element ,attributes)
      console.log("funcion link directiva 4");
    },
    compile:function () { //No inyectable (element ,attributes)
      console.log("funcion compile directiva 4");
      console.log(arguments);
      return {
        pre:function () { //NO inyectable (scope , element , attributes)
          console.log("función link desde prelink directiva 4");
          console.log(arguments);
        },
        post:function () { //NO inyectable (scope , element , attributes)
            console.log("función link desde postlink directiva 4");
            console.log(arguments);
        }
      };
    }
  };
}]);

myApp.directive('myDirectivecontroller',['$http',function ($http) {
  console.log('funcion inyectable descde directiva controller');
  return {
    restric: 'E',
    link:function () {//No inyectable (scope , element ,attributes)
      console.log("funcion link directiva controller");
    },
    compile:function () { //No inyectable (element ,attributes)
      console.log("funcion compile directiva controller");
      console.log(arguments);
      return function($scope,element,attrs,controller){// NO inyectable (scope,element,attribute)
        console.log("funcion link desde compile directive controller");
        console.log(arguments);
        controller.addPane();
      };
    },
    controller:function ($http,$scope) {
      console.log("function Controller de directiva controller");
      this.addPane=function () {
        console.log("Función Panel agragado");
      };
    }
  };
}]);


myApp.directive("myDirectivetemplate",["$http",function ($http) {
  console.log("funcion inyectable directive template");
  return {
    restrict:'E',
    // template:'<h1>Hi from template directive</h1>',
    templateUrl:function(element,attrs){
        if (attrs.t==="tem") {
          return 'template.html';
        }
    },
    replace:true
  };
}]);

myApp.directive("myDirectiveIsolated",function() {
  return {
    restrict:'E',
    scope:{
      oneBinding:'@oneBinding',
      twoBinding:'=twoBinding',
      click:'&function'
    },
    link:function (scope) {
      console.log(scope.oneBinding);
      console.log(scope.twoBinding);
      scope.click();
    }
  };
});

myApp.directive("myDirectiveEval",function () {
  return {
    restrict:'E',
    link:function (scope,_,attrs) {
      console.log(attrs.oneBinding);
      console.log(scope.$eval(attrs.twoBinding));
      scope.$eval(attrs.function);
    }
  };
});
