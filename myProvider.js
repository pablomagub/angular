myApp.provider("myProvider",function() {
  var defaultText = "Hello Wold from Provider!!!";
  var text = defaultText;

  this.setCulture = function(culture) {
    (culture === 'es') ? text = "Hola Mundo desde Provider!!!":defaultText;
  };

  this.$get = ['$http', function($http) {
    //devuelve a pelo una funcion que escribe en consola
    return {
      write: function () {
        console.log(text);
      }
    };
  }];
});
